import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE','data_edge.settings')

import django
django.setup()

from gamenetic.models import Vector2D,Dataset

dataset = Dataset.objects.create(name="mugs",num_float_features=1,kind="regression")

if (dataset):
    print("Dataset has been created")
else:
    print("Something went wrong")

#create a list of lists
measurements = [ [10,12], [9,12.5], [8.8,9.5], [8.5,11.5], [10,14], [7.9,13], [9.5,9.7], [8.7,9.6], [8.5,9.4], [7.8,11.2] ]

for m in measurements:
    #get the id
    unique_row = Dataset.objects.get(name="mugs")
    unique_row_id = unique_row.id
    # print(unique_row_id)
    vector_table_row = Vector2D.objects.create(dataset_id=unique_row_id,x=m[0],y=m[1])

