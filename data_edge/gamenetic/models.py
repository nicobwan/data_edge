from django.db import models



# Create your models here.

class Dataset(models.Model):
    name = models.TextField()
    num_float_features = models.IntegerField()
    kind = models.TextField()

class Vector2D(models.Model):
    #its not a one to one field
    dataset = models.ForeignKey(Dataset, on_delete=models.CASCADE ,unique = False)
    x = models.FloatField()
    y = models.FloatField()
