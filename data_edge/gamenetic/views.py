from django.shortcuts import render

# Create your views here.

from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from gamenetic.models import Dataset , Vector2D
from gamenetic.serializers import  DatasetSerializer,Vector2DSerializer

# Create your views here.

def index(request):
    return HttpResponse("Hello , Thanks for stopping by to learn Data Science")

class Datasetlist(APIView):
 
    def get(self,request):
        dataset = Dataset.objects.all()
        serializer = DatasetSerializer(dataset, many= True)
        return Response(serializer.data) # Return JSON
 
    def post(self):
        pass


class vector2Dlist(APIView):
 
    def get(self,request):
        vector2D = Vector2D.objects.all()
        serializer = Vector2DSerializer(vector2D, many= True)
        return Response(serializer.data) # Return JSON
 
    def post(self):
        pass
