from rest_framework import serializers
from gamenetic.models import   Dataset , Vector2D #import model
 
# Create a class
class DatasetSerializer(serializers.ModelSerializer):
 
    class Meta:
        model = Dataset
        fields = '__all__'

class Vector2DSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = Vector2D
		fields = '__all__'

