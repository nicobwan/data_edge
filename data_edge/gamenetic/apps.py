from django.apps import AppConfig


class GameneticConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'gamenetic'
