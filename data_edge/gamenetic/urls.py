from django.urls import path

from gamenetic import views

urlpatterns = [
    path('', views.index, name='index'),
    path('dataset/', views.Datasetlist.as_view()),
    path('vector2d/', views.vector2Dlist.as_view()),
]