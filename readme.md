# Internship Django Project 

## Installation of Django
+ pip is the recommended way to install Django
+ PIP is a python package installer 
+ Use the command below to install the latest stable Django version

$ python -m pip install Django

## Create the a django project
+ You must enter the directory where you want to store your code that is auto-generated inorder to establish a django_project just a collection of settings including the Database configurations and etc

$ django-admin startproject capstone_project

+ lets see what the "startproject" command created


+ the outer capstone_project/ - is a root directory of our project but django doesnot care about it so we can rename it anytime that we want to

manage.py/ - is a command line utiltiy that let's us interact with this Django project in various ways


+ the inner capstone_project - is the actual Python package for my project .its name is the Python package name i will need to use to import anything inside it 

+ capstone/__init__.py - is an empyty file that tells Python that this directory should be considered a Python package.

+ capstone/settings.py - the settings for this Django project

+ capstone/urls.py - the url declarations for this Django project 

+ capstone/asgi.py - this is an entry point for ASGI-compatible web servers to serve my project

+ capstone/wsgi.py - is an entry-point for the WSGI-compatible web servers to serve my project

## Creating the app
+ To create your app make sure you are in the same directory like that of manage.py

$ python manage.py startapp proai_playground

+ That creates a 'proai_playground', which is laid out like this

proai_playground/
	__init__.py
	admin.py
	apps.py
	migrations/
		__init__.py
	models.py
	tests.py
	views.py

This directory will hold the proai_playground application

## Create the first view
+ Open the proai_playground/views.py and put in the python code 

+ To call that view in Django ,we need to map it to a URL -and for that we need a url configuration

+ To create a URLconf in the proai_playground ,we create a file called urls.py.

+ In the proai_playground file we include the following code



+ Next is to point the new urls.py file in the app directory to the urls.py file project directory which we do using the following code



$ python manage.py runserver

## Create database and models
Open capstone_project/settings.py
We donot need to configure the database right now because ,we will be using SQLite which is with in python but as we progress we shall need a scalable database like mysql 

At the top of the file , we will notice the "INSTALLED_APPS" setting 
it contains the following apps
django.contrib.admin-The admin site
django.contrib.auth- An authentication system
django.contrib.contenttypes - A framework for content types
django.contrib.sessions - A session framework
django.contrib.messages - A messaging framework
django.contrib.staticfiles - A framework for managing static files

Some of the above apps make use of atleast one database table called

$ python manage.py migrate

## Creating models
Essentially these means creating our database layout with additional metadata

In our proai_playground app we shall have two tables one called Dataset and the other called Vector2D

Will in put this inside our proai_playground/models.py


Now each model here is represented by a class that subclasses django.db.models.Model . Each model has a number of class variables which represents a database field in the model.

## Activating the models
That model code give Django alot of information .With Django is able to create a database for this app
and also a python database-access API for accessing Question and Choice objects

But first we need to tell our project that proai_playground app is installed .

To include the app in our project, we need to add a reference to its configuration class in the INSTALLED_APPS setting. The PollsConfig class is in the polls/apps.py file, so its dotted path is 'polls.apps.PollsConfig'. Edit the mysite/settings.py file and add that dotted path to the INSTALLED_APPS setting. 


'proai_playground.apps.ProaiPlaygroundConfig'

Now that Django knows our proai app . let's run another command

$ python manage.py makemigrations proai_playground

The 'makemigrations ' command tells Django that you have made some changs to my models and that i would like those changes to be stored as a migration.

Then run 

$ python manage.py migrate

The 'migrate ' command takes all the migrations that haven't been appiled and runs them against my database


The three step guide is 

+ change my models(in models.py)
+ run python manage.py makemigrations to create migrations for those changes
+ run python manage.py migrate to apply those changes to the database


## Django Rest API FrameWork
REST API is an architectural design that have many constraints for designing web applications.

## Installing Django Rest Framework
$ pip install djangorestframework

## Add the rest framework to Django's installed apps
'rest_framework'

## Creating the Serializer class
This class is used to convert a model into JSON data

Create a new profile file called serializers in the application folder.


In this file, i will have to just request an API and get the JSON back.


+ HttpResponse which you have been using in any program to return the response.

+ get_object_or_404 will respond when requested object doesn’t exist.

+ APIView is used so that the normal views can return the API data.

+ Response is that where you can get status or a particular response. If everything will be fine then it will return 202 response.

+ status basically sends back the status.

+ Then import model and serializer.

+ Now you have to create a class based view which basically inherits from API view.

+ In datasetlist class you have to define two methods – get( ) that are used to return all the books which you wanted  and post( ) that helps into creating new books.

+ In get( ) method pass the request.

+ Now create a variable that will store all your objects. In my case it is dataset1.

+ Next you have to serialize them, that means it will take all the objects and convert them into JSON.

+ many = true means there are many of them so you don’t have return just one JSON object.

+ As we know every view function returns a HTTP response, in our case in JSON. So we have to return JSON.


## Requirements
+ Python 3.7.9
+ Django
+ Virtualenvironment

## Acknowledgements

+ [Writing Your First Django App tutorial](https://docs.djangoproject.com/en/3.2/intro/tutorial01/)
+ [Beginner's guide to understand Rest API ](https://www.simplifiedpython.net/django-rest-api-tutorial/)


